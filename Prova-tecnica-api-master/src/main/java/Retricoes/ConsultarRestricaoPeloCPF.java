package Retricoes;


import org.hamcrest.Matchers;
import org.junit.Test;

import Base.BaseAPI;
import static io.restassured.RestAssured.given;

public class ConsultarRestricaoPeloCPF extends BaseAPI {
	
	@Test
	public void ConsultarCpfSemRestricao() {
		
		given()
		.when()
		.get("/api/v1/restricoes/44750909878")
		.then()
		.statusCode(204);	
	}
	

	
	@Test
	public void ConsultarCpfComRestricao() {
		
		given()
		.when()
		.get("/api/v1/restricoes/97093236014")
		.then()
		.statusCode(200)
		.body("mensagem", Matchers.is("O CPF 97093236014 possui restricao"));	
		
	}
}
