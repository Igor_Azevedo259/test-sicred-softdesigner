package Simulacoes;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Test;

public class T5_RemoverUmaSimulacaoId {
	
	
	
	
	@Test
	public void RemoverUmaSimulacaoPeloId() {
		
		
		given()
		
		.when()
		
		.delete("/api/v1/simulacoes/11")
		
		.then()
		
		.statusCode(204);
		
		
	}
	
	
	
	@Test
	public void RemoverUmaSimulacaoNaoCadastrada() {
		

		given()
		
		.when()
		.delete("/api/v1/simulacoes/23")
		
		.then()
		
		.statusCode(404)
		
		.body("mensagem", Matchers.is("Simula��o nao encontrada"));
		
		
		
	}

}
