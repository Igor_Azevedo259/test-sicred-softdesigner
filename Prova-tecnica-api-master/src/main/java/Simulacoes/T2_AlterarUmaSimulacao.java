package Simulacoes;
import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import Base.BaseAPI;


public class T2_AlterarUmaSimulacao extends BaseAPI {

	
	@Test
	public void AlterarUmaSimulacao() {
		Map<String, String> request = new HashMap<>();

		request.put("nome", "Silvano Alencar");
		request.put("cpf", "4584830565");
		request.put("email", "silvaninhoalencar@gmail.com");
		request.put("valor", "4000");
		request.put("parcelas", "5");
		request.put("seguro", "false");
		
	
		given()
		
		.body(request)
		
		.when()
		
		.put("/api/v1/simulacoes/4584830333")
		
		.then()
		.statusCode(200)
		.body("nome", Matchers.is("Silvano Alencar"))
		.body("cpf", Matchers.is("4584830565"))
		.body("email", Matchers.is("silvaninhoalencar@gmail.com"))
		.body("valor", Matchers.is(4000))
		.body("parcelas", Matchers.is(5))
		.body("seguro", Matchers.is(false));
	}
	
	
	
	
	@Test
	public void AlterarSimulacaoNaoCadastrada () {
		given()
	
		.when()
		
		.put("/api/v1/simulacoes/4584989312")
		
		.then()
		.statusCode(404)
		.body("mensagem", Matchers.is("CPF nao encontrado"));
	}
	
}
