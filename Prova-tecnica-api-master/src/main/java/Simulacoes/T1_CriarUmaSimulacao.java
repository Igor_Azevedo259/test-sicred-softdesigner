package Simulacoes;
import Base.BaseAPI;


import static io.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;


public class T1_CriarUmaSimulacao extends BaseAPI {	
	
	
    @Test
	public void CriarSimulacao() {
		
	Map<String, String> request = new HashMap<>();

	request.put("nome", "Silvano");
	request.put("cpf", "4584830333");
	request.put("email", "silvaninho@gmail.com");
	request.put("valor", "3000");
	request.put("parcelas", "2");
	request.put("seguro", "true");
	
	
		given()
		
		.body(request)
		
		.when()
		
		.post("/api/v1/simulacoes")
		
		.then()
		.statusCode(201)
		
		.body("nome", Matchers.is("Silvano"))
		.body("cpf", Matchers.is("4584830333"))
		.body("email", Matchers.is("silvaninho@gmail.com"))
		.body("valor", Matchers.is(3000))
		.body("parcelas", Matchers.is(2))
		.body("seguro", Matchers.is(true));
		
		
	}
	
	@Test
	public void CriarSimulacaoComProblema() {
		Map<String, String> request = new HashMap<>();

		request.put("nome", "Silvano");
		request.put("cpf", "34568687695");
		request.put("email", "silvaninho@gmail.com");
		request.put("valor", "3000");
		request.put("parcelas", "1");
		request.put("seguro", "true");
		
		given()
		
		.body(request)
		
		.when()
		
		.post("/api/v1/simulacoes")
		
		.then()
		.statusCode(400)
		.body("erros.parcelas", Matchers.is("Parcelas deve ser igual ou maior que 2"));
	}
	

	
	
	
	@Test
	public void CriarSimulacaoComCpfJaExistente() {
		
		Map<String, String> request = new HashMap<>();

		request.put("nome", "Silvano");
		request.put("cpf", "4584830333");
		request.put("email", "silvaninho@gmail.com");
		request.put("valor", "3000");
		request.put("parcelas", "2");
		request.put("seguro", "true");
		
		given()
		
		.body(request)
		
		.when()
		
		.post("/api/v1/simulacoes")
		
		.then()
		.statusCode(409)
		.body("mensagem:", Matchers.is("CPF ja existente"))
		;
		
		
		
	}


}
	




