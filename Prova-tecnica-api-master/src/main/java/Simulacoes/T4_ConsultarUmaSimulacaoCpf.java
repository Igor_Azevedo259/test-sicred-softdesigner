package Simulacoes;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Test;

public class T4_ConsultarUmaSimulacaoCpf {


@Test
public void ConsultarUmaSimulacaoCpf() {
	
	given()
	
	.when()
	.get("/api/v1/simulacoes/4584830565")
	
	.then()
	
	.statusCode(200)
	
	.body("nome", Matchers.is("Silvano Alencar"))
	.body("cpf", Matchers.is("4584830565"));
	
	
}

@Test
public void ConsultarUmaSimulacaoNaoCadastrada() {
	
given()
	
	.when()
	.get("/api/v1/simulacoes/45697969594")
	
	.then()
	
	.statusCode(404);
	
}





}
