package Base;


import org.hamcrest.Matchers;
import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;


public class BaseAPI implements Constantes {

	
	@BeforeClass
	public static void setup() {
		
		RestAssured.baseURI = BASE_URL;
		RestAssured.basePath = BASE_PATH;
		
		
		RequestSpecBuilder req = new RequestSpecBuilder();
		req.setContentType(APP_CONTENTTYPE);
		RestAssured.requestSpecification = req.build();
		
		ResponseSpecBuilder resp = new ResponseSpecBuilder();
		resp.expectResponseTime(Matchers.lessThan(MAX_TIMEOUT));
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		
			
		
		
		
		
	}
}
